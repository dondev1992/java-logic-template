package io.catalyte.training;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Contains multiple common logic exercises.
 */
public class LogicExercise {

  /**
   * Takes a BigDecimal for the unit price and an int for number of units sold and returns a
   * discounted sales total based on the quantity sold: if more than 99 units are sold, apply a 15%
   * discount to the total price; if more than 49 units are sold, apply a 10% discount 10% to the
   * total price; if less than 50 units are sold, do not apply a discount to the price. For example,
   * if the unit price was 1.00 and the quantity sold was 100, the method should return 85.00 for
   * the total sales amount.
   */
  public BigDecimal getDiscount(BigDecimal unitPrice, int unitAmount) {
    double pricePerUnit = unitPrice.doubleValue();
    double unitsSold = (double) unitAmount;
    double subtotal = pricePerUnit * unitsSold;
    double total = subtotal;
     if (unitAmount >= 100) {
        total = subtotal - (subtotal * .15);
     } else if (unitAmount >= 50) {
       total = subtotal - (subtotal * .10);
     }
       return new BigDecimal(total).setScale(2, RoundingMode.HALF_DOWN);
  }

  /**
   * This method takes an int representing a percentile score and should return the appropriate
   * letter grade. If the score is above 90, return 'A'; if the score is between 80 and 89, return
   * 'B'; if the score is between 70 and 79, return 'C'; if the score is between 60 and 69, return
   * 'D'; if the score is below 60, return 'F'.
   */
  public char getGrade(int score) {
      if (score >= 90) {
        return 'A';
      } else if (score >= 80) {
        return 'B';
      } else if (score >= 70) {
        return 'C';
      } else if (score >= 60) {
        return 'D';
      }else {
        return 'F';
      }
  }

  /**
   * This method should take an ArrayList of strings, remove all the elements in the array
   * containing an even number of letters, and then return the result. For example, if given an
   * array of "Cat", "Dog", "Bird", the method should return an array containing only "Cat" and
   * "Dog".
   */
  public ArrayList<String> removeEvenLength(ArrayList<String> a) {
      a.removeIf(element -> (element.length() % 2 == 0));
    return a;
  }


  /**
   * This method should take an double array, a, and return a new array containing the square of
   * each element in a.
   */
  public double[] powerArray(double[] a) {
    double [] squaredArray = new double[a.length];
    for (int i = 0; i < a.length; i++) {
      a[i] *= a[i];
      squaredArray[i] = a[i];
    }
    return squaredArray;
  }


  /**
   * This method should take an int array, a, and return the index of the element with the greatest
   * value.
   */
  public int indexOfMax(int[] a) {

    if (a.length == 0) {
      return -1;
      }
    int index = 0;
    int maxElement = a[0];
      for (int i = 0; i < a.length; i++) {
          if (maxElement < a[i]) {
            maxElement = a[i];
            index = i;
          }
        }
    return index;
  }


  /**
   * This method should take an ArrayList of Integers, a, and returns true if all elements in the
   * array are divisible by the given int, i.
   */
  public boolean isDivisibleBy(ArrayList<Integer> a, int i) {
    ArrayList<Integer> elementsDivisibleByInteger = new ArrayList<>();
    boolean result = false;
    for (int index = 0; index < a.size(); index++) {
      if (a.get(index) % i == 0) {
        elementsDivisibleByInteger.add(a.get(index));
      }
    }
    if (a.equals(elementsDivisibleByInteger)) {
      result = true;
    }
    return result;
  }

  /**
   * A word is "abecedarian" if its letters appear in alphabetical order--the word 'biopsy' for
   * example. This method should take String s and return true if it is abecedarian.
   */
  public boolean isAbecedarian(String s) {
    s = s.toLowerCase();

    char[] characterArray = s.toCharArray();
    Arrays.sort(characterArray);
    String sortedCharacters = new String(characterArray);

    return sortedCharacters.equals(s);
  }

  /**
   * This method should take 2 strings and return true if they are anagrams of each other. For
   * example, "stop" is an anagram for "pots".
   */
  public boolean areAnagrams(String s1, String s2) {
      s1 = s1.toLowerCase();
      s2 = s2.toLowerCase();

    char[] characters1 = s1.toCharArray();
    char[] characters2 = s2.toCharArray();

    Arrays.sort(characters1);
    Arrays.sort(characters2);

    return Arrays.equals(characters1, characters2);
  }

  /**
   * This method should take a String and return the number of unique characters. For example, if
   * the method is given "noon", it should return a value of 2.
   */
  public int countUniqueCharacters(String s) {
    int count;
    char[] characterArray = s.toCharArray();
    ArrayList<Character> newStringList = new ArrayList<>();

    for (char character: characterArray) {
      if (!newStringList.contains(character)) {
        newStringList.add(character);
      }
    }
      count = newStringList.size();
      return count;
  }

  /**
   * This method should take a string and return true if it is a palindrome, i.e. it is spelled the
   * same forwards and backwards. For example, the words "racecar" and "madam" are palindromes.
   */
  public boolean isPalindrome(String s) {
    boolean result = false;
    s = s.toLowerCase();
    String testString = "";
    for (int i = s.length() - 1; i >= 0; i--) {
      testString += s.charAt(i);
    }
     if (testString.equals(s)) {
       result = true;
     }
    return result;
  }

  /**
   * This method should take a string and return a HashMap which is a map of characters to a list of
   * their indices in a string (i.e., which characters occur where in a string). For example for the
   * string "Hello World", the map would look something like: d=[9], o=[4, 6], r=[7], W=[5], H=[0],
   * l=[2, 3, 8], e=[1].
   */
  public HashMap<String, ArrayList<Integer>> concordanceForString(String s) {
    HashMap<String, ArrayList<Integer>> finalHashMap = new HashMap<>();
      char[] separatedParts = s.toCharArray();

    for (int i = 0; i < separatedParts.length; i++) {
      String letter = Character.toString(separatedParts[i]) ;
      ArrayList<Integer> indices = new ArrayList<>();
      indices.add(i);

      if (finalHashMap.containsKey(letter)) {
        finalHashMap.get(letter).add(i);
      } else {
        finalHashMap.put(letter, indices);
      }
    }
    return finalHashMap;

  }

}